import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class SvocGenerator extends Thread {
	int test = 0;
	int runid = 0;
	int threadid = 0;
	int batchcnt = 0;
	
	long select_time = 0;
	long var_time = 0;
	long batch_time = 0;
	long exec_time=0;
	
	
	String QryCustomerLookup = null;
	String queryStringGN[][] = null;
	String svocpatternGN[][] = null;
	String svocentitypatternGN[][] = null;
	
	SvocVariables sv;
	Connection con;

	public SvocGenerator(int threadid, Connection con, int run_id, String qry, String[][] queryStringGN_t,
			String[][] svocpatternGN_t, String[][] svocentitypatternGN_t) throws Exception {
		try {
		this.threadid = threadid;
		this.con = con;
		
		runid = run_id;
		QryCustomerLookup = qry;
		queryStringGN = queryStringGN_t;
		svocpatternGN = svocpatternGN_t;
		svocentitypatternGN = svocentitypatternGN_t;
		sv = new SvocVariables();
		}catch(Exception e)
		{
			System.out.println(" SvocGenerator Thread "+threadid+" error "+e.getMessage());
		
		}

	}

	@Override
	public void run() {
		
		System.out.println(" Thread " + threadid + " Starting "
				+ new SimpleDateFormat(" MM-dd-hh.mm.ss.SSS").format(Calendar.getInstance().getTime()));
				
		Instant initialtime = Instant.now();
		Instant threadinitialtime = Instant.now();

		
		int qsloopcheck = 0;
		int patloopcheck = 0;
		int numberinsert = 0;
		int numberinsert_bdder = 0;
		int log_customer_id = 0;
		int rs_cnt_fav_incr = 0;
		int rs_cnt_apfav_incr = 0;
		int rs_cnt_cpfav_incr = 0;
		int rs_cnt_ltdfav_incr = 0;
		int rs_cnt = 0;
		
		HashSet<String> log_variable_name = new HashSet<String>();
		HashSet<String> log_error = new HashSet<String>();

		String var_SVOC_Variable_Name = null;
		String var_SVOC_Pattern = null;
		String var_CDM_Entity = null;
		String var_CDM_Attribute = null;

		try {

			String Query = null;
			String Entity = null;
			ResultSet rs_ds = null;
			UtilFunctions utf = new UtilFunctions();
			
			Class.forName("com.mysql.jdbc.Driver");
			Statement stmt_ds = con.createStatement();
			

			Connection con_log = DriverManager.getConnection(CONSTANT.url, CONSTANT.username, CONSTANT.password);
			Connection con_pv = DriverManager.getConnection(CONSTANT.url, CONSTANT.username, CONSTANT.password);
			Connection con_tp = DriverManager.getConnection(CONSTANT.url, CONSTANT.username, CONSTANT.password);
			Connection con_cp = DriverManager.getConnection(CONSTANT.url, CONSTANT.username, CONSTANT.password);
			Connection con_ap = DriverManager.getConnection(CONSTANT.url, CONSTANT.username, CONSTANT.password);
			Connection con_fdv = DriverManager.getConnection(CONSTANT.url, CONSTANT.username, CONSTANT.password);
			Connection con_fsv = DriverManager.getConnection(CONSTANT.url, CONSTANT.username, CONSTANT.password);
			Connection con_bdder = DriverManager.getConnection(CONSTANT.url, CONSTANT.username, CONSTANT.password);

			con_log.setAutoCommit(false);
			con_pv.setAutoCommit(false);
			con_tp.setAutoCommit(false);
			con_cp.setAutoCommit(false);
			con_ap.setAutoCommit(false);
			con_fdv.setAutoCommit(false);
			con_fsv.setAutoCommit(false);
			con_bdder.setAutoCommit(false);

			Statement stmt_log = con_log.createStatement();
			Statement stmt_pv = con_pv.createStatement();
			Statement stmt_tp = con_tp.createStatement();
			Statement stmt_cp = con_cp.createStatement();
			Statement stmt_ap = con_ap.createStatement();
			Statement stmt_fdv = con_fdv.createStatement();
			Statement stmt_fsv = con_fsv.createStatement();
			Statement stmt_bdder = con_bdder.createStatement();

			Statement stmt_cust_lookup = con.createStatement();
			ResultSet cust_lookup = stmt_cust_lookup.executeQuery(QryCustomerLookup);
			
			stmt_log.executeUpdate("insert into svoc_log(customer_id,variable_name,error_desc) values('" + runid
					+ "','SVOC_Status','CDM Customer Lookup loaded Successful')");

			//System.out.println(" Customer Vairable Loop Start "+ new SimpleDateFormat(" MM-dd-hh.mm.ss.SSS").format(Calendar.getInstance().getTime()));
			
			
			Instant initialtimeDebugCustomer = null;
			int num_bdder = CONSTANT.bacth_insert_count;
			long totaldbtime=0;
			long totalexectime=0;
			long totalselectquerytime=0;
			long totalselectquerycount=0;
			Instant time=null;
			Instant time_1=null;
			
			Instant entity_time=null;
			Instant entity_level_time=null;
			Instant customer_time=null;
			Instant entity_var_time=null;
			Instant entity_addBatch_time=null;
			Instant entity_execBatch_time=null;
			
			while (cust_lookup.next()) {
				customer_time = Instant.now();
				int var_customer_id = cust_lookup.getInt("Customer_Id");
				
				try {
			
			/*	if (CONSTANT.DEBUG )
				{
					System.out.println("Thread :" + threadid + " DEBUG POSITION 1. CustomerId:"+cust_lookup.getInt("Customer_Id") + " Time: "
							+ Duration.between(threadinitialtime, Instant.now()).toMillis());
				}	
				*/	   
					qsloopcheck = 0;
					numberinsert++;

					// Looping for Entity
					while (utf.getQueryString(queryStringGN, qsloopcheck, 0) != null) {
						rs_cnt_fav_incr = 0;
						rs_cnt_apfav_incr = 0;
						rs_cnt_cpfav_incr = 0;
						rs_cnt_ltdfav_incr = 0;
						
						
						String Cust_Parti="";
						
						if(CONSTANT.PARTITION)
						{
						long part =(var_customer_id/CONSTANT.PARTITION_VALUE)+1;
						Cust_Parti=" partition(p"+part+") ";
						}
						entity_time = Instant.now();

						try {
							if (utf.getQueryString(queryStringGN, qsloopcheck, 0) == null) {
								break;
							}
							Entity = utf.getQueryString(queryStringGN, qsloopcheck, 1);

							if (Entity.equalsIgnoreCase("CDM_Bill_Header")) {
								Query = utf.getQueryString(queryStringGN, qsloopcheck, 0) +" "+ Cust_Parti+" where Customer_Id='"
										+ var_customer_id + "' ";
							}else if (Entity.equalsIgnoreCase("CDM_Bill_Details")) {
								Query = utf.getQueryString(queryStringGN, qsloopcheck, 0)+" "+ Cust_Parti + " where Customer_Id='"
										+ var_customer_id + "' ";
							}else if (!Entity.equalsIgnoreCase("CDM_Feedback")) {
								Query = utf.getQueryString(queryStringGN, qsloopcheck, 0) + " where Customer_Id='"
										+ var_customer_id + "' ";
							} else {
								Query = utf.getQueryString(queryStringGN, qsloopcheck, 0) + " where Fdbk_Customer_Id='"
										+ var_customer_id + "'";
							}
							time_1= Instant.now();
							if (Entity.equalsIgnoreCase("CDM_Non_Sales_Bill_Details")) {
							//	Query = Query + " and Product_Id <> -1 order by Bill_date desc limit 2 ";
								Query = Query + " and Product_Id <> -1   limit 2 ";
							}
							
							
								if (Entity.equalsIgnoreCase("CDM_Bill_Header")) {
								try {
									time= Instant.now();
								//	entity_level_time = Instant.now();
									rs_ds = stmt_ds.executeQuery(
											"select max(Bill_date),count(1) from CDM_Bill_Header where Customer_Id='"
													+ var_customer_id + "'");
									rs_ds.next();
									totalselectquerytime=totalselectquerytime+Duration.between(time, Instant.now()).toMillis();
									totalselectquerycount++;
									
								/*	if (CONSTANT.DEBUG )
									{
										System.out.println(" CDM_Bill_Header  max query time taken "
												+ Duration.between(entity_level_time, Instant.now()).toMillis());
									}	
									
									*/
									if(rs_ds.getInt(2)>20000)
									{
										System.out.println("Customer id : "+var_customer_id +" has "+rs_ds.getInt(2)+ " Bills it will casuing memory issue ");
										break;
									}
									sv.lt_fav_var_bhl = new String[rs_ds.getInt(2)][14];

									sv.ap_fav_var_bhl = new String[rs_ds.getInt(2)][14];
									sv.cp_fav_var_bhl = new String[rs_ds.getInt(2)][14];

									sv.ltd = rs_ds.getDate(1);
								} catch (Exception e) {
									sv.lt_fav_var_bhl = new String[5][14];
									sv.ap_fav_var_bhl = new String[5][14];
									sv.cp_fav_var_bhl = new String[5][14];

								}

							}
							
								
							if (Entity.equalsIgnoreCase("CDM_Bill_Header")
									|| Entity.equalsIgnoreCase("CDM_Bill_Details")) {
								//Query = Query + " order by Bill_Date asc ";
								Query = Query + "   ";
							}

							if (Entity.equalsIgnoreCase("CDM_Customer_Membership ")) {
							//	Query = Query + " order by Tier_Updated_Date desc ";
								Query = Query + "   ";
							}

						time = Instant.now();
						entity_level_time = Instant.now();
						
							rs_ds = stmt_ds.executeQuery(Query);
							
							totalselectquerytime=totalselectquerytime+Duration.between(time, Instant.now()).toMillis();
							totalselectquerycount++;
							
						/*	
						
							if (CONSTANT.DEBUG )
							{
								System.out.println(Entity+" Entity  time taken "
										+ Duration.between(entity_level_time, Instant.now()).toMillis());
							}
							
						*/	
							
							
							sv.customer_id = var_customer_id;
							svocentitypatternGN = utf.getEntityPattern(Entity, svocpatternGN);

							if (Entity.equalsIgnoreCase("CDM_Bill_Details")) {
								try {
									rs_ds.last();
									rs_cnt = rs_ds.getRow();
									sv.lt_fav_var = new String[rs_cnt][14];
									sv.ap_fav_var = new String[rs_cnt][14];
									sv.cp_fav_var = new String[rs_cnt][14];
									sv.ltd_fav_var = new String[rs_cnt][14];
									rs_ds.beforeFirst();
								} catch (Exception e) {
									sv.lt_fav_var = new String[5][14];
									sv.ap_fav_var = new String[5][14];
									sv.cp_fav_var = new String[5][14];
									sv.ltd_fav_var = new String[5][14];
									rs_ds.beforeFirst();

								}
							}
				      if (CONSTANT.DEBUG )
							{
						//	System.out.println("Query :"+Query);
							System.out.println(" "+Entity+" Select Query Time taken  : "+Duration.between(entity_time, Instant.now()).toMillis()); 
					
							}	
				      
				      select_time = select_time + Duration.between(entity_level_time, Instant.now()).toMillis();
						
				      	
				      entity_var_time = Instant.now();
			// Each Customer for a Entity
							while (rs_ds.next()) {

								patloopcheck = 0;
								while (utf.getSvocpattern(svocentitypatternGN, patloopcheck, 0) != null) {
									try {
										if (utf.getSvocpattern(svocentitypatternGN, patloopcheck, 0) == null) {
											break;
										}
										var_SVOC_Variable_Name = utf.getSvocpattern(svocentitypatternGN, patloopcheck,
												0);
										var_SVOC_Pattern = utf.getSvocpattern(svocentitypatternGN, patloopcheck, 1);
										var_CDM_Entity = utf.getSvocpattern(svocentitypatternGN, patloopcheck, 2);
										var_CDM_Attribute = utf.getSvocpattern(svocentitypatternGN, patloopcheck, 3);

										
										// if column values is null not required to check
										if (rs_ds.getString(var_CDM_Attribute) != null) {
											try {
												switch (var_SVOC_Variable_Name) {

												case "Is_Valid_Customer":
													try {
														sv.is_valid_customer = rs_ds.getInt(var_CDM_Attribute);
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "Is_Employee":

													try {
														if (rs_ds.getString(var_CDM_Attribute).toLowerCase()
																.contains("@gmail.com")) {
															sv.is_employee = 0;

														} else if (rs_ds.getString(var_CDM_Attribute).toLowerCase()
																.contains("@yahoo.com")) {

															sv.is_employee = 0;
														} else if (rs_ds.getString(var_CDM_Attribute).toLowerCase()
																.contains("@live.com")) {

															sv.is_employee = 0;
														} else {
															if (rs_ds.getString(var_CDM_Attribute).length() > 3) {
																sv.is_employee = 1;
															}
														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "Registration_Date":
													try {
														sv.registration_date = rs_ds.getDate(var_CDM_Attribute);
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "Registration_Year":
													try {
														sv.registration_year = rs_ds.getInt(var_CDM_Attribute);
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "Is_Valid_Email":
													try {
														sv.is_valid_email = utf
																.isValidMail(rs_ds.getString(var_CDM_Attribute));
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "Is_Valid_Mobile":
													try {
														sv.is_valid_mobile = utf
																.isValidMobile(rs_ds.getString(var_CDM_Attribute));
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "Is_Valid_Pincode":

													try {
														sv.is_valid_pincode = rs_ds.getString(var_CDM_Attribute);
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "Is_Valid_Dob":
													try {
														sv.is_valid_dob = utf
																.getAge(rs_ds.getDate(var_CDM_Attribute)) < 100 ? 1 : 0;
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "Tier":
													try {
														sv.tier = rs_ds.getInt(var_CDM_Attribute);
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "Previous_Tier":

													try {
														if (sv.tier != rs_ds.getInt(var_CDM_Attribute)
																&& sv.previous_tier == -1) {
															sv.previous_tier = rs_ds.getInt(var_CDM_Attribute);
														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "Tier_Change_Date":
													try {

														if (sv.tier_change_date == null) {
															sv.tier_change_date = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.previous_tier == -1) {
															// if
															// (sv.tier_change_date.compareTo(rs_ds.getDate(var_CDM_Attribute))
															// <= 0) {
															sv.tier_change_date = rs_ds.getDate(var_CDM_Attribute);
															// }
														}

													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "Spend_Since_Last_Tier_Change":
													try {
														if (sv.tier_change_date
																.compareTo(rs_ds.getDate("Bill_Date")) <= 0) {
															sv.spend_since_last_tier_change
																	.add(rs_ds.getDouble(var_CDM_Attribute));
														}
													} catch (Exception e) {
													}
													break;

												case "Percent_To_Next_Tier":
													try {
														sv.percent_to_next_tier = rs_ds.getInt("Percent_To_Next_Tier");
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												// Start CDM_Customer_Time_Dependent_Var
												// variables
												case "Age":
													try {
														sv.age = utf.getAge(rs_ds.getDate(var_CDM_Attribute));
														// Age_Band
														if (CONSTANT.ageBandDic.get(sv.age) != null) {
															sv.age_band =  (Integer) CONSTANT.ageBandDic.get(sv.age);
														} else {
															sv.age_band = 0;
														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "FTD":
													try {
														if (sv.ftd == null) {
															sv.ftd = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.ftd.compareTo(rs_ds.getDate(var_CDM_Attribute)) >= 0) {
															sv.ftd = rs_ds.getDate(var_CDM_Attribute);
															// Vintage
															if (sv.ftd != null) {
																Date td = new Date();

																int difference = Integer.parseInt(
																		((td.getTime() - sv.ftd.getTime()) / 86400000)
																				+ "");
																sv.vintage = difference;
															}
															// Tenure
															if (sv.ftd != null && sv.ltd != null) {

																int difference = Integer
																		.parseInt(((sv.ltd.getTime() - sv.ftd.getTime())
																				/ 86400000) + "");

																sv.tenure = difference;

															}
															// Vintage_Band
															if (CONSTANT.vintageDic.get(sv.vintage) != null) {
																sv.vintage_band =  (Integer) CONSTANT.vintageDic
																		.get(sv.vintage);
															} else {
																sv.vintage_band = 0;
															}

														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "LTD":
													try {
														if (sv.ltd == null) {
															sv.ltd = rs_ds.getDate(var_CDM_Attribute);
														}

														if (sv.ltd.compareTo(rs_ds.getDate(var_CDM_Attribute)) <= 0) {
															sv.ltd = rs_ds.getDate(var_CDM_Attribute);

															// Recency
															if (sv.ltd != null) {
																Date td = new Date();
																int difference = Integer.parseInt(
																		((td.getTime() - sv.ltd.getTime()) / 86400000)
																				+ "");
																sv.recency = difference;
															}
															// Recency_Band
															if (CONSTANT.recencyDic.get(sv.recency) != null) {
																sv.recency_band =   (Integer) CONSTANT.recencyDic
																		.get(sv.recency);
															} else {
																sv.recency_band = 0;
															}

															// Tenure
															if (sv.ftd != null && sv.ltd != null) {

																int difference = Integer
																		.parseInt(((sv.ltd.getTime() - sv.ftd.getTime())
																				/ 86400000) + "");

																sv.tenure = difference;

															}

														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "STD":
													try {
														if (sv.std == null) {
															if (sv.ftd
																	.compareTo(rs_ds.getDate(var_CDM_Attribute)) < 0) {
																sv.std = rs_ds.getDate(var_CDM_Attribute);
															}
														}
														if (sv.ftd.compareTo(rs_ds.getDate(var_CDM_Attribute)) < 0) {
															if (sv.std
																	.compareTo(rs_ds.getDate(var_CDM_Attribute)) > 0) {
																sv.std = rs_ds.getDate(var_CDM_Attribute);
															}

														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												// CDM_Customer_TP_Var

												case "LT_Trans":

													sv.lt_trans.add(rs_ds.getString(var_CDM_Attribute));

													break;

												case "LT_Rev":
													sv.lt_rev.add(rs_ds.getDouble(var_CDM_Attribute));
													break;

												case "LT_Qty":
													sv.lt_qty.add(rs_ds.getDouble(var_CDM_Attribute));
													break;

												case "LT_Disc_Percent":
													sv.lt_disc_percent.add(rs_ds.getDouble(var_CDM_Attribute));
													break;

												case "LT_Num_Of_Visits":
													sv.lt_num_of_visits.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "LT_GMV":
													sv.lt_gmv.add(rs_ds.getDouble(var_CDM_Attribute));
													break;

												case "LT_Bday_Trans_Cnt":
													try {
														sv.lt_bday_trans_cnt.add(utf.isTransact(sv.dob,
																rs_ds.getDate(var_CDM_Attribute), 15));

													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "LT_Anniv_Trans_Cnt":
													try {
														sv.lt_anniv_trans_cnt.add(utf.isTransact(sv.doa,
																rs_ds.getDate(var_CDM_Attribute), 15));
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												// Points Variable

												case "LT_Pts_Redeem_Count":
													sv.lt_pts_redeem_count.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "LT_Pts_Redeem":
													sv.lt_pts_redeem.add(rs_ds.getDouble(var_CDM_Attribute));
													break;

												case "LT_Pts_Earned":
													sv.lt_pts_earned.add(rs_ds.getDouble(var_CDM_Attribute));
													break;

												case "LT_Pts_Redeem_FD":
													try {
														if (sv.lt_pts_redeem_fd == null) {
															sv.lt_pts_redeem_fd = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.lt_pts_redeem_fd
																.compareTo(rs_ds.getDate(var_CDM_Attribute)) >= 0) {

															sv.lt_pts_redeem_fd = rs_ds.getDate(var_CDM_Attribute);

														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "LT_Pts_Redeem_LD":
													try {
														if (sv.lt_pts_redeem_ld == null) {
															sv.lt_pts_redeem_ld = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.lt_pts_redeem_ld
																.compareTo(rs_ds.getDate(var_CDM_Attribute)) <= 0) {
															sv.lt_pts_redeem_ld = rs_ds.getDate(var_CDM_Attribute);

														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "LT_Next_Pts_Expiry_Date":
													try {
														if (sv.lt_next_pts_expiry_date == null) {
															if ((new Date())
																	.compareTo(rs_ds.getDate(var_CDM_Attribute)) < 0) {
																sv.lt_next_pts_expiry_date = rs_ds
																		.getDate(var_CDM_Attribute);
															}
														}
														if ((new Date())
																.compareTo(rs_ds.getDate(var_CDM_Attribute)) < 0) {
															if (sv.lt_next_pts_expiry_date
																	.compareTo(rs_ds.getDate(var_CDM_Attribute)) > 0) {
																sv.lt_next_pts_expiry_date = rs_ds
																		.getDate(var_CDM_Attribute);
															}

														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "LT_Percent_Bills_With_Point_Redemption":
													sv.lt_percent_billss_with_point_redemption
															.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "LT_Fav_Day_Of_Week":
													sv.lt_fav_day_of_week.add(rs_ds.getString(var_CDM_Attribute));
													try {
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][0] = rs_ds
																.getString(var_CDM_Attribute) == null ? "00"
																		: rs_ds.getString(var_CDM_Attribute);
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][1] = rs_ds
																.getString(var_CDM_Attribute) == null
																		? "00"
																		: rs_ds.getString(var_CDM_Attribute)
																				.replaceAll("[0-4]+", "0")
																				.replaceAll("[5-7]+", "1");

														sv.lt_fav_var_bhl[rs_cnt_fav_incr][2] = (String) CONSTANT.strStore
																.get(rs_ds.getString("Store_Id")) == null ? "00"
																		: (String) CONSTANT.strStore
																				.get(rs_ds.getString("Store_Id"));

														sv.lt_fav_var_bhl[rs_cnt_fav_incr][3] = (String) CONSTANT.strFormat
																.get(rs_ds.getString("Store_Id")) == null ? "00"
																		: (String) CONSTANT.strFormat
																				.get(rs_ds.getString("Store_Id"));

														// Used for Store Fav
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][4] = rs_ds
																.getString("Bill_Header_Id");
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][5] = rs_ds
																.getString("Bill_Total_Val");
														
														// Is_Ever_StarLoyal
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][6] = rs_ds.getString("Bill_Date");
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][7] = "00";
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][8] = "00";
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][9] = "00";
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][10] = "00";

														sv.lt_fav_var_bhl[rs_cnt_fav_incr][11] = "1";
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][12] = "1";
														sv.lt_fav_var_bhl[rs_cnt_fav_incr][13] = rs_ds
																.getString("Bill_Date");

														rs_cnt_fav_incr++;

													} catch (Exception e) {
														System.out.println(sv.customer_id + " == " + rs_cnt_fav_incr
																+ " LTD Favorite Store " + e);
														System.out.println(" "+e.getMessage());
													}
													break;

												case "LT_Fav_Timebands_Of_Day":
													sv.lt_fav_timebands_of_day.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "LT_WeekEnd_WeekDay":
													sv.lt_weekend_weekday.add(rs_ds.getString(var_CDM_Attribute)
															.replaceAll("[0-4]+", "0").replaceAll("[5-7]+", "1"));
													break;

												// Distinct count
												case "LT_Distinct_Prod_Cnt":
													sv.lt_distinct_prod_cnt
															.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "LT_Distinct_Brand_Cnt":
													sv.lt_distinct_brand_cnt.add((String) CONSTANT.prdBrand
															.get(rs_ds.getString(var_CDM_Attribute)));
													break;

												case "LT_Distinct_Divison_Cnt":
													sv.lt_distinct_divison_cnt.add((String) CONSTANT.prdDiv
															.get(rs_ds.getString(var_CDM_Attribute)));
													break;

												case "LT_Distinct_Dep_Cnt":
													sv.lt_distinct_dep_cnt.add((String) CONSTANT.prdDep
															.get(rs_ds.getString(var_CDM_Attribute)));
													break;

												case "LT_Distinct_Cat_Cnt":
													sv.lt_distinct_cat_cnt.add((String) CONSTANT.prdCat
															.get(rs_ds.getString(var_CDM_Attribute)));
													break;

												case "LT_Distinct_Season_Cnt":
													sv.lt_distinct_season_cnt.add((String) CONSTANT.prdSeas
															.get(rs_ds.getString(var_CDM_Attribute)));
													break;

												case "LT_Distinct_BU_Cnt":
													sv.lt_distinct_bu_cnt.add((String) CONSTANT.prdBU
															.get(rs_ds.getString(var_CDM_Attribute)));
													break;

												// Favourite
												case "LT_Fav_Prod_Id":
													try {
														sv.is_new_product = sv.lt_fav_prod_id.size() == 0 ? true
																: !sv.lt_fav_prod_id
																		.contains(CONSTANT.prdProduct.get(
																				rs_ds.getString(var_CDM_Attribute)));

														sv.product_lov_id = (String) CONSTANT.prdProduct
																.get(rs_ds.getString(var_CDM_Attribute));
														
														sv.lt_fav_var[rs_cnt_fav_incr][13] = rs_ds
																.getString("Bill_Date");
														
														
														
														
														
														sv.lt_fav_prod_id.add((String) CONSTANT.prdProduct
																.get(rs_ds.getString(var_CDM_Attribute)));

														// sv.lt_fav_size.add(rs_ds.getString(var_CDM_Attribute));
														sv.is_premium_prod = (CONSTANT.prdPremium
																.get(rs_ds.getString(var_CDM_Attribute)) == "-1" ? "0"
																		: "1");
														try {
															sv.lt_fav_var[rs_cnt_fav_incr][0] = (String) CONSTANT.prdProduct
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdProduct
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][1] = (String) CONSTANT.prdBU
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdBU
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][2] = (String) CONSTANT.prdBrand
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdBrand
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][3] = (String) CONSTANT.prdCat
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdCat
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][4] = (String) CONSTANT.prdDep
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdDep
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][5] = (String) CONSTANT.prdDiv
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdDiv
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][6] = (String) CONSTANT.prdSeas
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdSeas
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][7] = (String) CONSTANT.prdSec
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdSec
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][8] = (String) CONSTANT.prdManuf
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdManuf
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][9] = (String) CONSTANT.prdCustomTag
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdCustomTag
																					.get(rs_ds.getString("Product_Id"));
															sv.lt_fav_var[rs_cnt_fav_incr][10] = "00";

															sv.lt_fav_var[rs_cnt_fav_incr][11] = rs_ds
																	.getString("Sale_Net_Val");
															sv.lt_fav_var[rs_cnt_fav_incr][12] = rs_ds
																	.getString("Sale_Qty");
															sv.lt_fav_var[rs_cnt_fav_incr][13] = rs_ds
																	.getString("Bill_Date");
															

															// System.out.println(sv.lt_fav_var[rs_cnt_fav_incr][0]+" --
															// "+sv.lt_fav_var[rs_cnt_fav_incr][1]);
															rs_cnt_fav_incr++;

														} catch (Exception e) {
															System.out.println(sv.customer_id + " == " + rs_cnt_fav_incr
																	+ " Favorite " + e);
															System.out.println(" "+e.getMessage());
														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "LT_Fav_Brand_Id":

													sv.is_new_brand = sv.lt_fav_brand_id.size() == 0 ? true
															: !sv.lt_fav_brand_id.contains(CONSTANT.prdBrand
																	.get(rs_ds.getString(var_CDM_Attribute)));
													sv.lt_fav_brand_id.add((String) CONSTANT.prdBrand
															.get(rs_ds.getString(var_CDM_Attribute)));

													sv.is_premium_brand = (CONSTANT.prdPremiumBrand
															.get(rs_ds.getString(var_CDM_Attribute)) == "-1" ? "0"
																	: "1");
													sv.brand_lov_id = (String) CONSTANT.prdBrand
															.get(rs_ds.getString(var_CDM_Attribute));

													break;

												case "LT_Fav_Divison_Id":

													sv.is_new_div = sv.lt_fav_divison_id.size() == 0 ? true
															: !sv.lt_fav_divison_id.contains(CONSTANT.prdDiv
																	.get(rs_ds.getString(var_CDM_Attribute)));

													sv.lt_fav_divison_id.add((String) CONSTANT.prdDiv
															.get(rs_ds.getString(var_CDM_Attribute)));

													sv.div_lov_id = (String) CONSTANT.prdDiv
															.get(rs_ds.getString(var_CDM_Attribute));

													break;

												case "LT_Fav_Dep_Id":
													sv.is_new_dep = sv.lt_fav_dep_id.size() == 0 ? true
															: !sv.lt_fav_dep_id.contains(CONSTANT.prdDep
																	.get(rs_ds.getString(var_CDM_Attribute)));

													sv.lt_fav_dep_id.add((String) CONSTANT.prdDep
															.get(rs_ds.getString(var_CDM_Attribute)));

													sv.dep_lov_id = (String) CONSTANT.prdDep
															.get(rs_ds.getString(var_CDM_Attribute));

													break;

												case "LT_Fav_Cat_Id":
													sv.is_new_cat = sv.lt_fav_cat_id.size() == 0 ? true
															: !sv.lt_fav_cat_id.contains(CONSTANT.prdCat
																	.get(rs_ds.getString(var_CDM_Attribute)));
													sv.lt_fav_cat_id.add((String) CONSTANT.prdCat
															.get(rs_ds.getString(var_CDM_Attribute)));
													sv.cat_lov_id = (String) CONSTANT.prdCat
															.get(rs_ds.getString(var_CDM_Attribute));
													break;

												case "LT_Fav_Season_Id":
													sv.is_new_season = sv.lt_fav_season_id.size() == 0 ? true
															: !sv.lt_fav_season_id.contains(CONSTANT.prdSeas
																	.get(rs_ds.getString(var_CDM_Attribute)));
													sv.lt_fav_season_id.add((String) CONSTANT.prdSeas
															.get(rs_ds.getString(var_CDM_Attribute)));
													sv.season_lov_id = (String) CONSTANT.prdSeas
															.get(rs_ds.getString(var_CDM_Attribute));

													break;

												case "LT_Fav_Section_Id":
													sv.is_new_section = sv.lt_fav_section_id.size() == 0 ? true
															: !sv.lt_fav_section_id.contains(CONSTANT.prdSec
																	.get(rs_ds.getString(var_CDM_Attribute)));
													sv.lt_fav_section_id.add((String) CONSTANT.prdSec
															.get(rs_ds.getString(var_CDM_Attribute)));
													sv.section_lov_id = (String) CONSTANT.prdSec
															.get(rs_ds.getString(var_CDM_Attribute));

													break;

												case "LT_Fav_Manuf_Id":

													sv.is_new_manuf = sv.lt_fav_manf_id.size() == 0 ? true
															: !sv.lt_fav_manf_id.contains(CONSTANT.prdManuf
																	.get(rs_ds.getString(var_CDM_Attribute)));

													sv.lt_fav_manf_id.add((String) CONSTANT.prdManuf
															.get(rs_ds.getString(var_CDM_Attribute)));
													sv.manuf_lov_id = (String) CONSTANT.prdManuf
															.get(rs_ds.getString(var_CDM_Attribute));
													break;

												case "LT_Fav_BU_Id":
													sv.is_new_bu = sv.lt_fav_bu_id.size() == 0 ? true
															: !sv.lt_fav_bu_id.contains(CONSTANT.prdBU
																	.get(rs_ds.getString(var_CDM_Attribute)));
													sv.lt_fav_bu_id.add((String) CONSTANT.prdBU
															.get(rs_ds.getString(var_CDM_Attribute)));
													sv.bu_lov_id = (String) CONSTANT.prdBU
															.get(rs_ds.getString(var_CDM_Attribute));

													break;

												// Store

												case "LT_Fav_Store_Id":

													boolean present = sv.lt_fav_store_id.size() == 0 ? true
															: !sv.lt_fav_store_id
																	.contains(rs_ds.getString(var_CDM_Attribute));

													sv.is_new_store.put(rs_ds.getInt("Bill_Header_Id"), present);

													// if(!((String) CONSTANT.strStore
													// .get(rs_ds.getString(var_CDM_Attribute))).equals("-1"))
													// {
													
													sv.lt_fav_store_id.add((String) CONSTANT.strStore
															.get(rs_ds.getString(var_CDM_Attribute)));
													sv.store_id = rs_ds.getString(var_CDM_Attribute);
													
													// }
													break;

												case "LT_Fav_Store_Location":

													boolean presentloc = sv.lt_fav_store_location.size() == 0 ? true
															: !sv.lt_fav_store_location
																	.contains(CONSTANT.strCityName
																			.get(rs_ds.getString(var_CDM_Attribute)));

													sv.txn_in_new_city.put(rs_ds.getInt("Bill_Header_Id"), presentloc);

													sv.lt_fav_store_location.add((String) CONSTANT.strCityName
															.get(rs_ds.getString(var_CDM_Attribute)));

													break;

												case "LT_Fav_Store_Format":
													sv.lt_fav_store_format.add((String) CONSTANT.strFormat
															.get(rs_ds.getString(var_CDM_Attribute)));
													break;

												case "FTD_Store_Id":
													try {
														if (sv.ftd != null) {
															if (sv.ftd.compareTo(rs_ds.getDate("bill_date")) == 0) {
																sv.ftd_store_id = (String) CONSTANT.strStore
																		.get(rs_ds.getString(var_CDM_Attribute));
															}
														}
													} catch (Exception e) {
													}
													break;

												case "LTD_Store_Id":
													try {
														if (sv.ltd != null) {
															if (sv.ltd.compareTo(rs_ds.getDate("bill_date")) == 0) {
																sv.ltd_store_id = (String) CONSTANT.strStore
																		.get(rs_ds.getString(var_CDM_Attribute));

															}
														}
													} catch (Exception e) {
													}
													break;

												// LFR Variable

												case "LFR_Date":
													try {
														if (sv.lfr_date == null) {
															sv.lfr_date = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.lfr_date
																.compareTo(rs_ds.getDate(var_CDM_Attribute)) <= 0) {
															sv.lfr_date = rs_ds.getDate(var_CDM_Attribute);
														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "LFR_Overall":
													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_overall = rs_ds.getString(var_CDM_Attribute);
													}
													break;

												case "LFR_Product_Variety":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_product_variety = rs_ds.getString(var_CDM_Attribute);
													}

													break;

												case "LFR_Product_Quality":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_product_quality = rs_ds.getString(var_CDM_Attribute);
													}

													break;

												case "LFR_Product_Pricing":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_product_pricing = rs_ds.getString(var_CDM_Attribute);
													}

													break;

												case "LFR_Product_Relevance":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_product_relevance = rs_ds.getString(var_CDM_Attribute);
													}

													break;

												case "LFR_Product_Availability":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_product_availability = rs_ds
																.getString(var_CDM_Attribute);
													}

													break;

												case "LFR_Store":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_store = rs_ds.getString(var_CDM_Attribute);
													}

													break;

												case "LFR_Service":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_service = rs_ds.getString(var_CDM_Attribute);
													}

													break;

												case "LFR_L1RSN":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_l1rsn = rs_ds.getString(var_CDM_Attribute);
													}

													break;

												case "LFR_Store_LOV_Id":

													if (sv.lfr_date.compareTo(rs_ds.getDate("Fdbk_Survey_Date")) == 0) {
														sv.lfr_store_lov_id = rs_ds.getString(var_CDM_Attribute);
													}

													break;

												case "Last_Comm_Date":
													try {
														if (sv.last_comm_date == null) {
															sv.last_comm_date = utf.StringDateFormat(
																	rs_ds.getString(var_CDM_Attribute), "yyyyMMdd");

														}
														if (sv.last_comm_date.compareTo(utf.StringDateFormat(
																rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) > 0) {

															sv.last_comm_date = utf.StringDateFormat(
																	rs_ds.getString(var_CDM_Attribute), "yyyyMMdd");

														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "Last_Comm_Template_Code":

													if (sv.last_comm_date.compareTo(utf.StringDateFormat(
															rs_ds.getString("Event_Execution_Date_ID"),
															"yyyyMMdd")) == 0) {

														sv.last_comm_template_code = rs_ds.getInt(var_CDM_Attribute);

													}

													break;

												case "Last_Voucher_Id":
													if (sv.last_comm_date.compareTo(utf.StringDateFormat(
															rs_ds.getString("Event_Execution_Date_ID"),
															"yyyyMMdd")) == 0) {

														sv.last_voucher_id = rs_ds.getString(var_CDM_Attribute);
														sv.offer_code = rs_ds.getString(var_CDM_Attribute);

													}

													break;

												case "Current_Voucher_Expiry_Date":
													try {
														if (sv.last_comm_date.compareTo(utf.StringDateFormat(
																rs_ds.getString("Event_Execution_Date_ID"),
																"yyyyMMdd")) == 0) {

															sv.current_voucher_expiry_date = utf.StringDateFormat(
																	rs_ds.getString(var_CDM_Attribute), "yyyyMMdd");

															Date cd = new Date();
															if (cd.compareTo(rs_ds.getDate("Module_Date")) > 0) {
																sv.module_event_execution_date = rs_ds
																		.getDate("Module_Date");
															}

														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}

													break;

												case "No_Of_SMS_In_Last_7Days":
													if (utf.dayFor(-7).compareTo(utf.StringDateFormat(
															rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) < 0)
														sv.no_of_sms_in_last_7days
																.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "No_Of_SMS_In_Last_30Days":
													if (utf.dayFor(-7).compareTo(utf.StringDateFormat(
															rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) < 0)
														sv.no_of_sms_in_last_30days
																.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "No_Of_SMS":
													if (rs_ds.getString(var_CDM_Attribute) != null) {
														sv.lt_no_of_sms.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "No_Of_EMAIL_In_Last_7Days":
													if (utf.dayFor(-7).compareTo(utf.StringDateFormat(
															rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) < 0)
														sv.no_of_email_in_last_7days
																.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "No_Of_EMAIL_In_Last_30Days":
													if (utf.dayFor(-7).compareTo(utf.StringDateFormat(
															rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) < 0)
														sv.no_of_email_in_last_30days
																.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "No_Of_EMAIL":
													if (rs_ds.getString(var_CDM_Attribute) != null) {
														sv.lt_no_of_email.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "No_Of_PN_In_Last_7Days":
													if (utf.dayFor(-7).compareTo(utf.StringDateFormat(
															rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) < 0)
														sv.no_of_pn_in_last_7days
																.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "No_Of_PN_In_Last_30Days":
													if (utf.dayFor(-7).compareTo(utf.StringDateFormat(
															rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) < 0)
														sv.no_of_pn_in_last_30days
																.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "No_Of_PN":
													if (rs_ds.getString(var_CDM_Attribute) != null) {
														sv.lt_no_of_pn.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "No_Of_Comm_In_Last_7Days":
													if (utf.dayFor(-7).compareTo(utf.StringDateFormat(
															rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) < 0)
														sv.no_of_comm_in_last_7days
																.add(rs_ds.getString(var_CDM_Attribute));
													break;

												case "No_Of_Comm_In_Last_30Days":
													if (utf.dayFor(-30).compareTo(utf.StringDateFormat(
															rs_ds.getString(var_CDM_Attribute), "yyyyMMdd")) < 0)
														sv.no_of_comm_in_last_30days
																.add(rs_ds.getString(var_CDM_Attribute));
													break;
												case "LT_No_Of_Comm":
													if (rs_ds.getString(var_CDM_Attribute) != null) {
														sv.lt_no_of_comm.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												// Active Period Variable

												case "AP_Trans":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_trans.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "AP_Rev":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_rev.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "AP_Qty":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_qty.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "AP_Disc_Percent":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_disc_percent.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "AP_Num_Of_Visits":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_num_of_visits.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "AP_GMV":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_gmv.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "AP_Bday_Trans_Cnt":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_bday_trans_cnt.add(utf.isTransact(sv.dob,
																rs_ds.getDate(var_CDM_Attribute), 15));
													}
													break;

												case "AP_Anniv_Trans_Cnt":
													try {
														if ("Active_Period"
																.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
															sv.ap_anniv_trans_cnt.add(utf.isTransact(sv.doa,
																	rs_ds.getDate(var_CDM_Attribute), 15));
														}
													} catch (Exception e) {
														System.out.println(" "+e.getMessage());
													}
													break;

												case "AP_Pts_Redeem":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_pts_redeem.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "AP_Pts_Earned":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_pts_earned.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "AP_Pts_Redeem_FD":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														if (sv.ap_pts_redeem_fd == null) {
															sv.ap_pts_redeem_fd = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.ap_pts_redeem_fd
																.compareTo(rs_ds.getDate(var_CDM_Attribute)) >= 0) {

															sv.ap_pts_redeem_fd = rs_ds.getDate(var_CDM_Attribute);

														}
													}
													break;

												case "AP_Pts_Redeem_LD":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														if (sv.ap_pts_redeem_ld == null) {
															sv.ap_pts_redeem_ld = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.ap_pts_redeem_ld
																.compareTo(rs_ds.getDate(var_CDM_Attribute)) <= 0) {
															sv.ap_pts_redeem_ld = rs_ds.getDate(var_CDM_Attribute);

														}
													}
													break;

												case "AP_Percent_Bills_With_Point_Redemption":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_percent_billss_with_point_redemption
																.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "AP_Fav_Day_Of_Week":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_day_of_week.add(rs_ds.getString(var_CDM_Attribute));

														try {
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][0] = rs_ds
																	.getString(var_CDM_Attribute) == null ? "00"
																			: rs_ds.getString(var_CDM_Attribute);
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][1] = rs_ds
																	.getString(var_CDM_Attribute) == null
																			? "00"
																			: rs_ds.getString(var_CDM_Attribute)
																					.replaceAll("[0-4]+", "0")
																					.replaceAll("[5-7]+", "1");

															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][2] = (String) CONSTANT.strStore
																	.get(rs_ds.getString("Store_Id")) == null ? "00"
																			: (String) CONSTANT.strStore
																					.get(rs_ds.getString("Store_Id"));

															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][3] = (String) CONSTANT.strFormat
																	.get(rs_ds.getString("Store_Id")) == null ? "00"
																			: (String) CONSTANT.strFormat
																					.get(rs_ds.getString("Store_Id"));

															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][4] = rs_ds
																	.getString("Bill_Header_Id");
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][5] = rs_ds
																	.getString("Bill_Total_Val");
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][6] = "00";
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][7] = "00";
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][8] = "00";
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][9] = "00";
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][10] = "00";

															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][11] = "1";
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][12] = "1";
															sv.ap_fav_var_bhl[rs_cnt_apfav_incr][13] = rs_ds
																	.getString("Bill_Date");

															rs_cnt_apfav_incr++;

														} catch (Exception e) {
															System.out.println(sv.customer_id + " == "
																	+ rs_cnt_apfav_incr + " AP Favorite Store " + e);

														}

													}
													break;

												case "AP_Fav_Timebands_Of_Day":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_timebands_of_day
																.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "AP_WeekEnd_WeekDay":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_weekend_weekday.add(rs_ds.getString(var_CDM_Attribute)
																.replaceAll("[0-4]+", "0").replaceAll("[5-7]+", "1"));
													}
													break;
													
													// AP_Online_Num_Of_Visits
												case "AP_Online_Num_Of_Visits":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														if (CONSTANT.online_source_program.equalsIgnoreCase(rs_ds.getString(var_CDM_Attribute)))
														{
														sv.ap_online_num_of_visits.add(rs_ds.getString("Bill_Date"));
														}
													}
													break;

												// Distinct count
												case "AP_Distinct_Prod_Cnt":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_distinct_prod_cnt
																.add(rs_ds.getString(var_CDM_Attribute));
														
														
													}
													break;

												case "AP_Distinct_Brand_Cnt":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_distinct_brand_cnt.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Distinct_Divison_Cnt":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_distinct_divison_cnt.add((String) CONSTANT.prdDiv
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Distinct_Dep_Cnt":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_distinct_dep_cnt.add((String) CONSTANT.prdDep
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Distinct_Cat_Cnt":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_distinct_cat_cnt.add((String) CONSTANT.prdCat
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Distinct_Season_Cnt":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_distinct_season_cnt.add((String) CONSTANT.prdSeas
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Distinct_BU_Cnt":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_distinct_bu_cnt.add((String) CONSTANT.prdBU
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												// Favourite
												case "AP_Fav_Prod_Id":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_prod_id.add(rs_ds.getString(var_CDM_Attribute));
														try {
															sv.ap_fav_var[rs_cnt_apfav_incr][0] = (String) CONSTANT.prdProduct
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdProduct
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][1] = (String) CONSTANT.prdBU
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdBU
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][2] = (String) CONSTANT.prdBrand
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdBrand
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][3] = (String) CONSTANT.prdCat
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdCat
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][4] = (String) CONSTANT.prdDep
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdDep
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][5] = (String) CONSTANT.prdDiv
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdDiv
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][6] = (String) CONSTANT.prdSeas
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdSeas
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][7] = (String) CONSTANT.prdSec
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdSec
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][8] = (String) CONSTANT.prdManuf
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdManuf
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][9] = (String) CONSTANT.prdCustomTag
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdCustomTag
																					.get(rs_ds.getString("Product_Id"));
															sv.ap_fav_var[rs_cnt_apfav_incr][10] = "00";

															sv.ap_fav_var[rs_cnt_apfav_incr][11] = rs_ds
																	.getString("Sale_Net_Val");
															sv.ap_fav_var[rs_cnt_apfav_incr][12] = rs_ds
																	.getString("Sale_Qty");
															sv.ap_fav_var[rs_cnt_apfav_incr][13] = rs_ds
																	.getString("Bill_Date");

															// System.out.println(sv.lt_fav_var[rs_cnt_fav_incr][0]+" --
															// "+sv.lt_fav_var[rs_cnt_fav_incr][1]);
															rs_cnt_apfav_incr++;

														} catch (Exception e) {
															System.out.println(sv.customer_id + " == "
																	+ rs_cnt_apfav_incr + " AP Favorite " + e);
														}

													}
													break;

												case "AP_Fav_Brand_Id":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_brand_id.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Fav_Divison_Id":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_divison_id.add((String) CONSTANT.prdDiv
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Fav_Dep_Id":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_dep_id.add((String) CONSTANT.prdDep
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Fav_Cat_Id":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_cat_id.add((String) CONSTANT.prdCat
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Fav_Season_Id":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_season_id.add((String) CONSTANT.prdSeas
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "AP_Fav_BU_Id":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_bu_id.add((String) CONSTANT.prdBU
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												// Store

												case "AP_Fav_Store_Id":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_store_id.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "AP_Fav_Store_Format":
													if ("Active_Period"
															.equalsIgnoreCase(rs_ds.getString("Active_Period"))) {
														sv.ap_fav_store_format.add((String) CONSTANT.strFormat
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												// Customer Period Variable

												case "CP_Trans":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_trans.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "CP_Rev":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_rev.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "CP_Qty":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_qty.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "CP_Disc_Percent":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_disc_percent.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "CP_Num_Of_Visits":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_num_of_visits.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "CP_GMV":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_gmv.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "CP_Bday_Trans_Cnt":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_bday_trans_cnt.add(utf.isTransact(sv.dob,
																rs_ds.getDate(var_CDM_Attribute), 15));
													}
													break;

												case "CP_Anniv_Trans_Cnt":
													try {
														if ("1".equalsIgnoreCase(utf.isTransactCP(sv.ltd,
																rs_ds.getDate("Bill_Date"), (365 - 15)))) {
															sv.cp_anniv_trans_cnt.add(utf.isTransactCP(sv.doa,
																	rs_ds.getDate(var_CDM_Attribute), 15));
														}
													} catch (Exception e) {
														// System.out.println(e);
													}
													break;

												case "CP_Pts_Redeem":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Redemption_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_pts_redeem.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "CP_Pts_Earned":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Earned_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_pts_earned.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "CP_Pts_Redeem_FD":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Redemption_Date"),
																	(CONSTANT.current_period_days)))) {
														if (sv.cp_pts_redeem_fd == null) {
															sv.cp_pts_redeem_fd = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.cp_pts_redeem_fd
																.compareTo(rs_ds.getDate(var_CDM_Attribute)) >= 0) {

															sv.cp_pts_redeem_fd = rs_ds.getDate(var_CDM_Attribute);

														}
													}
													break;

												case "CP_Pts_Redeem_LD":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Redemption_Date"),
																	(CONSTANT.current_period_days)))) {
														if (sv.cp_pts_redeem_ld == null) {
															sv.cp_pts_redeem_ld = rs_ds.getDate(var_CDM_Attribute);
														}
														if (sv.cp_pts_redeem_ld
																.compareTo(rs_ds.getDate(var_CDM_Attribute)) < 0) {
															sv.cp_pts_redeem_ld = rs_ds.getDate(var_CDM_Attribute);

														}
													}
													break;

												case "CP_Percent_Bills_With_Point_Redemption":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Redemption_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_percent_billss_with_point_redemption
																.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "CP_Fav_Day_Of_Week":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_day_of_week.add(rs_ds.getString(var_CDM_Attribute));
														try {
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][0] = rs_ds
																	.getString(var_CDM_Attribute) == null ? "00"
																			: rs_ds.getString(var_CDM_Attribute);
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][1] = rs_ds
																	.getString(var_CDM_Attribute) == null
																			? "00"
																			: rs_ds.getString(var_CDM_Attribute)
																					.replaceAll("[0-4]+", "0")
																					.replaceAll("[5-7]+", "1");

															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][2] = (String) CONSTANT.strStore
																	.get(rs_ds.getString("Store_Id")) == null ? "00"
																			: (String) CONSTANT.strStore
																					.get(rs_ds.getString("Store_Id"));

															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][3] = (String) CONSTANT.strFormat
																	.get(rs_ds.getString("Store_Id")) == null ? "00"
																			: (String) CONSTANT.strFormat
																					.get(rs_ds.getString("Store_Id"));

															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][4] = "00";
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][5] = "00";
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][6] = "00";
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][7] = "00";
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][8] = "00";
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][9] = "00";
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][10] = "00";

															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][11] = "1";
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][12] = "1";
															sv.cp_fav_var_bhl[rs_cnt_cpfav_incr][13] = rs_ds
																	.getString("Bill_Date");

															rs_cnt_cpfav_incr++;

														} catch (Exception e) {
															System.out.println(sv.customer_id + " == "
																	+ rs_cnt_cpfav_incr + " LTD Favorite Store " + e);

														}
													}
													break;

												case "CP_Fav_Timebands_Of_Day":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_timebands_of_day
																.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "CP_WeekEnd_WeekDay":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_weekend_weekday.add(rs_ds.getString(var_CDM_Attribute)
																.replaceAll("[0-4]+", "0").replaceAll("[5-7]+", "1"));
													}
													break;

												// Distinct count
												case "CP_Distinct_Prod_Cnt":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_distinct_prod_cnt
																.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "CP_Distinct_Brand_Cnt":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_distinct_brand_cnt.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Distinct_Divison_Cnt":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_distinct_divison_cnt.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Distinct_Dep_Cnt":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_distinct_dep_cnt.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Distinct_Cat_Cnt":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_distinct_cat_cnt.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Distinct_Season_Cnt":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_distinct_season_cnt.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Distinct_BU_Cnt":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_distinct_bu_cnt.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												// Favourite
												case "CP_Fav_Prod_Id":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_prod_id.add(rs_ds.getString(var_CDM_Attribute));

														try {
															sv.cp_fav_var[rs_cnt_cpfav_incr][0] = (String) CONSTANT.prdProduct
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdProduct
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][1] = (String) CONSTANT.prdBU
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdBU
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][2] = (String) CONSTANT.prdBrand
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdBrand
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][3] = (String) CONSTANT.prdCat
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdCat
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][4] = (String) CONSTANT.prdDep
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdDep
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][5] = (String) CONSTANT.prdDiv
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdDiv
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][6] = (String) CONSTANT.prdSeas
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdSeas
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][7] = (String) CONSTANT.prdSec
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdSec
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][8] = (String) CONSTANT.prdManuf
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdManuf
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][9] = (String) CONSTANT.prdCustomTag
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdCustomTag
																					.get(rs_ds.getString("Product_Id"));
															sv.cp_fav_var[rs_cnt_cpfav_incr][10] = "00";

															sv.cp_fav_var[rs_cnt_cpfav_incr][11] = rs_ds
																	.getString("Sale_Net_Val");
															sv.cp_fav_var[rs_cnt_cpfav_incr][12] = rs_ds
																	.getString("Sale_Qty");
															sv.cp_fav_var[rs_cnt_cpfav_incr][13] = rs_ds
																	.getString("Bill_Date");

															// System.out.println(sv.lt_fav_var[rs_cnt_cpfav_incr][0]+"
															// -- "+sv.lt_cpfav_var[rs_cnt_cpfav_incr][1]);
															rs_cnt_cpfav_incr++;

														} catch (Exception e) {
															System.out.println(sv.customer_id + " == "
																	+ rs_cnt_cpfav_incr + " CP Favorite " + e);
														}

													}
													break;

												case "CP_Fav_Brand_Id":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_brand_id.add((String) CONSTANT.prdBrand
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Fav_Divison_Id":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_divison_id.add((String) CONSTANT.prdDiv
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Fav_Dep_Id":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_dep_id.add((String) CONSTANT.prdDep
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Fav_Cat_Id":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_cat_id.add((String) CONSTANT.prdCat
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Fav_Season_Id":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_season_id.add((String) CONSTANT.prdSeas
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "CP_Fav_BU_Id":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_bu_id.add((String) CONSTANT.prdBU
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												// Store

												case "CP_Fav_Store_Id":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_store_id.add(rs_ds.getString(var_CDM_Attribute));
													}
													break;

												case "CP_Fav_Store_Format":
													if ("1".equalsIgnoreCase(
															utf.isTransactCP(sv.ltd, rs_ds.getDate("Bill_Date"),
																	(CONSTANT.current_period_days)))) {
														sv.cp_fav_store_format.add((String) CONSTANT.strFormat
																.get(rs_ds.getString(var_CDM_Attribute)));
													}
													break;

												case "YTD":
													if ("YTD_Current".equalsIgnoreCase(rs_ds.getString("YTD"))) {
														sv.ytd_rev_current_cal_year
																.add(rs_ds.getDouble(var_CDM_Attribute));
														sv.ytd_trans_current_cal_year
																.add(rs_ds.getString("Bill_Header_Id"));
														sv.ytd_num_of_visits_current_cal_year
																.add(rs_ds.getString("Bill_Date"));
													} else if ("YTD_Last".equalsIgnoreCase(rs_ds.getString("YTD"))) {
														sv.ytd_rev_last_cal_year
																.add(rs_ds.getDouble(var_CDM_Attribute));
														sv.ytd_trans_last_cal_year
																.add(rs_ds.getString("Bill_Header_Id"));
														sv.ytd_num_of_visits_last_cal_year
																.add(rs_ds.getString("Bill_Date"));
													}
													break;

												case "Tier_Expiry_Date":
													if (sv.tier_expiry_date == null) {
														sv.tier_expiry_date = rs_ds.getDate(var_CDM_Attribute);
													}

													if (sv.tier_expiry_date
															.compareTo(rs_ds.getDate(var_CDM_Attribute)) <= 0) {
														sv.tier_expiry_date = rs_ds.getDate(var_CDM_Attribute);
													}
													break;

												case "Rev_Tier_365Days":
													if ("1".equalsIgnoreCase(utf.isTransactCP(sv.tier_expiry_date,
															rs_ds.getDate("Bill_Date"), 365))) {
														sv.rev_bwn_curr_tiear_exp_minus_365days
																.add(rs_ds.getDouble(var_CDM_Attribute));
													}
													break;

												case "Last_Product_Return_Date":
													if (sv.last_product_return_date == null) {
														sv.last_product_return_date = rs_ds.getDate(var_CDM_Attribute);
													}
													if (sv.last_product_return_date
															.compareTo(rs_ds.getDate(var_CDM_Attribute)) <= 0) {
														sv.last_product_return_date = rs_ds.getDate(var_CDM_Attribute);

													}
													break;

												case "Is_Last_Txn":
													try {
														sv.bill_details_id = rs_ds.getInt("Bill_Details_Id");
														sv.bill_header_id = rs_ds.getInt("Bill_Header_Id");
														sv.product_id = rs_ds.getString("Product_Id");
														sv.bill_date = rs_ds.getDate("Bill_Date");
														

														sv.txn_during_festival_lov_id = rs_ds
																.getInt("Txn_During_Festival_LOV_Id");
														sv.txn_during_brand_event_lov_id = rs_ds
																.getInt("Txn_During_Brand_Event_LOV_Id");

														int bir_trans = utf.isTransact(sv.dob,
																rs_ds.getDate(var_CDM_Attribute), 15) == "0" ? 0 : 1;
														int ann_trans = utf.isTransact(sv.doa,
																rs_ds.getDate(var_CDM_Attribute), 15) == "0" ? 0 : 1;

														sv.txn_during_personal_event_lov_id = (bir_trans == 0
																? ann_trans
																: bir_trans);

														if (sv.ltd.compareTo(rs_ds.getDate(var_CDM_Attribute)) == 0) {
															sv.is_last_txn = 1;
															
															if(sv.is_new_cat)
															{
															  sv.ltd_product_id = rs_ds.getString("Product_Id");
															  sv.ltd_product_name =  (String) CONSTANT.prdProductName
																.get(rs_ds.getString("Product_Id")) == null ? "00"
																		: (String) CONSTANT.prdProductName
																				.get(rs_ds.getString("Product_Id"));
															}
															
														}
															
														if (CONSTANT.cdm_bill_details_derived_attr == 1) {
															
															
															
															stmt_bdder.addBatch(
																	utf.Cacl_CDM_Bill_Details_Derived_Attr(sv));
															numberinsert_bdder++;
															
														}
													} catch (Exception e) {
														// System.out.println("Insert query
														// Cacl_CDM_Bill_Details_Derived_Attr "+e.getMessage());

													}

													if (sv.ltd.compareTo(rs_ds.getDate(var_CDM_Attribute)) <= 0) {
														try {
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][0] = (String) CONSTANT.prdProduct
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdProduct
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][1] = (String) CONSTANT.prdBU
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdBU
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][2] = (String) CONSTANT.prdBrand
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdBrand
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][3] = (String) CONSTANT.prdCat
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdCat
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][4] = (String) CONSTANT.prdDep
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdDep
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][5] = (String) CONSTANT.prdDiv
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdDiv
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][6] = (String) CONSTANT.prdSeas
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdSeas
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][7] = (String) CONSTANT.prdSec
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdSec
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][8] = (String) CONSTANT.prdManuf
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdManuf
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][9] = (String) CONSTANT.prdCustomTag
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdCustomTag
																					.get(rs_ds.getString("Product_Id"));
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][10] = (String) CONSTANT.prdProductAge
																	.get(rs_ds.getString("Product_Id")) == null ? "00"
																			: (String) CONSTANT.prdProductAge
																					.get(rs_ds.getString("Product_Id"));;

															sv.ltd_fav_var[rs_cnt_ltdfav_incr][11] = rs_ds
																	.getString("Sale_Net_Val");
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][12] = rs_ds
																	.getString("Sale_Qty");
															sv.ltd_fav_var[rs_cnt_ltdfav_incr][13] = rs_ds
																	.getString("Bill_Date");

															// System.out.println("Test "+rs_cnt_ltdfav_incr+ " ==
															// "+sv.ltd_fav_var[rs_cnt_ltdfav_incr][0]+" --
															// "+sv.ltd_fav_var[rs_cnt_ltdfav_incr][1]);
															rs_cnt_ltdfav_incr++;

														} catch (Exception e) {
															System.out.println(
																	sv.customer_id + " == " + rs_cnt_ltdfav_incr
																			+ " LTD Favorite " + e.getMessage());

														}

													}

													break;
												}

											} catch (Exception e) {
												log_customer_id = sv.customer_id;
												log_variable_name.add(var_SVOC_Variable_Name);
												log_error.add(e.toString());
											}

											// SVOC variable calculation End here
											patloopcheck++;
										} // end if for null check
										else {
											patloopcheck++;
										}
									} catch (Exception e) {
										// System.out.println("Before Switch "+e);
										patloopcheck++;
										// break;
									}
								}

							}
							 if (CONSTANT.DEBUG )
								{
								//System.out.println("Query :"+Query+" Time taken  : start "+entity_time+" End : "+Instant.now()+" Duration :"+Duration.between(entity_time, Instant.now()).toMillis()); 
								System.out.println("    "+Entity+" Variable Calucation Time taken  : "+Duration.between(entity_var_time, Instant.now()).toMillis()); 
						
								}
							 
							   var_time = var_time + Duration.between(entity_var_time, Instant.now()).toMillis();
							qsloopcheck++;
						} catch (Exception e) {
							// System.out.println("Entity Lookup "+e);
							qsloopcheck++;
							// break;
						}
						
						
					}
					
					
					
					//	 System.out.println( " Customer Vairable loop ends "+new SimpleDateFormat(" MM-dd-hh.mm.ss").format(Calendar.getInstance().getTime()));
					entity_addBatch_time= Instant.now();
					Instant addbatch_time = Instant.now();
					try {

						if (CONSTANT.cdm_customer_profile_var == 1)
							stmt_pv.addBatch(utf.Cacl_CDM_Customer_Profile_Var(sv));
					} catch (Exception e) {
						log_customer_id = sv.customer_id;
						log_variable_name.add(var_SVOC_Variable_Name);
						log_error.add(e.toString());

					}
					if(CONSTANT.DEBUG)
					{
					System.out.println(" Add to Batch for  cdm_customer_profile_var Time taken : "
							+ Duration.between(entity_addBatch_time, Instant.now())
									.toMillis());
					}
					entity_addBatch_time= Instant.now();
					

					try {
						if (CONSTANT.cdm_customer_tp_var == 1)
							stmt_tp.addBatch(utf.Cacl_CDM_Customer_TP_Var(sv));
					} catch (Exception e) {
						log_customer_id = sv.customer_id;
						log_variable_name.add(var_SVOC_Variable_Name);
						log_error.add(e.toString());
					}
					
					if(CONSTANT.DEBUG)
					{
					System.out.println(" Add to Batch for  cdm_customer_tp_var Time taken : "
							+ Duration.between(entity_addBatch_time, Instant.now())
									.toMillis());
					}
					entity_addBatch_time= Instant.now();

					try {

						if (CONSTANT.cdm_customer_ap_var == 1)
							stmt_ap.addBatch(utf.Cacl_CDM_Customer_AP_Var(sv));

					} catch (Exception e) {
						log_customer_id = sv.customer_id;
						log_variable_name.add(var_SVOC_Variable_Name);
						log_error.add(e.toString());

					}
					
					if(CONSTANT.DEBUG)
					{
					System.out.println(" Add to Batch for  cdm_customer_ap_var Time taken : "
							+ Duration.between(entity_addBatch_time, Instant.now())
									.toMillis());
					}
					entity_addBatch_time= Instant.now();


					try {

						if (CONSTANT.cdm_customer_cp_var == 1)
							stmt_cp.addBatch(utf.Cacl_CDM_Customer_CP_Var(sv));

					} catch (Exception e) {
						log_customer_id = sv.customer_id;
						log_variable_name.add(var_SVOC_Variable_Name);
						log_error.add(e.toString());
					}
					
					if(CONSTANT.DEBUG)
					{
					System.out.println(" Add to Batch for  CDM_Customer_CP_Var Time taken : "
							+ Duration.between(entity_addBatch_time, Instant.now())
									.toMillis());
					}
					entity_addBatch_time= Instant.now();


					try {

						if (CONSTANT.cdm_customer_feedback_var == 1)
							stmt_fdv.addBatch(utf.Cacl_CDM_Customer_Feedback_Var(sv));

					} catch (Exception e) {
						log_customer_id = sv.customer_id;
						log_variable_name.add(var_SVOC_Variable_Name);
						log_error.add(e.toString());
					}
					
					if(CONSTANT.DEBUG)
					{
					System.out.println(" Add to Batch for  cdm_customer_feedback_var Time taken : "
							+ Duration.between(entity_addBatch_time, Instant.now())
									.toMillis());
					}
					entity_addBatch_time= Instant.now();


					try {
						if (CONSTANT.cdm_customer_fav_store_var == 1)
							stmt_fsv.addBatch(utf.Cacl_CDM_Customer_Fav_Store_Var(sv));

					} catch (Exception e) {
						log_customer_id = sv.customer_id;
						log_variable_name.add(var_SVOC_Variable_Name);
						log_error.add(e.toString());
					}
					if(CONSTANT.DEBUG)
					{
					System.out.println(" Add to Batch for  cdm_customer_fav_store_var Time taken : "
							+ Duration.between(entity_addBatch_time, Instant.now())
									.toMillis());
					}
					batch_time = batch_time + Duration.between(addbatch_time, Instant.now()).toMillis();	
					
					
					
					if(batchcnt==0)
					{
						batchcnt=CONSTANT.bacth_insert_count;
					}
					try {
						
					if (numberinsert_bdder >= batchcnt) {
						
						entity_execBatch_time= Instant.now();
						initialtime = Instant.now();
						
						batchcnt=batchcnt+CONSTANT.bacth_insert_count;
						
						stmt_bdder.executeBatch();
						con_bdder.commit();
						stmt_bdder.clearBatch();
						if(CONSTANT.DEBUG)
						{
						
						System.out.println(" Execute  Batch Time taken : "
								+ Duration.between(entity_execBatch_time, Instant.now())
										.toMillis());
						
					}
						
						exec_time=exec_time+ Duration.between(entity_execBatch_time, Instant.now())
						.toMillis();
						
						System.out.println("Thread " + threadid
																		+ " Total Bills Persisted :"
																		+ numberinsert_bdder + " Batch Size :"
																		+ CONSTANT.bacth_insert_count + " Time : "
																		+ Duration.between(initialtime, Instant.now())
																				.toMillis());
											totaldbtime=totaldbtime+Duration.between(initialtime, Instant.now()).toMillis();
					}
					} catch (Exception e) {
						

					}


					if ((numberinsert % CONSTANT.bacth_insert_count) == 0) {
						initialtime = Instant.now();
						entity_execBatch_time= Instant.now();
						
						try {
							stmt_pv.executeBatch();
							stmt_tp.executeBatch();
							stmt_cp.executeBatch();
							stmt_ap.executeBatch();
							stmt_fdv.executeBatch();
							stmt_fsv.executeBatch();
						} catch (Exception e) {
							System.out.println("Warring in ExecuteBatch " + e.getMessage());
						}

						con_log.commit();
						con_pv.commit();
						con_tp.commit();
						con_cp.commit();
						con_ap.commit();
						con_fdv.commit();
						con_fsv.commit();

						stmt_pv.clearBatch();
						stmt_tp.clearBatch();
						stmt_cp.clearBatch();
						stmt_ap.clearBatch();
						stmt_fdv.clearBatch();
						stmt_fsv.clearBatch();
						if(CONSTANT.DEBUG)
						{
						System.out.println(" Execute  Batch Time taken : "
								+ Duration.between(entity_execBatch_time, Instant.now())
										.toMillis());
						}
						exec_time=exec_time+ Duration.between(entity_execBatch_time, Instant.now())
						.toMillis();

System.out.println("Thread :" + threadid + " Total Customers Persisted:" + numberinsert
								+ " Batch Size :" + CONSTANT.bacth_insert_count + " Time: "
								+ Duration.between(initialtime, Instant.now()).toMillis());
						
						totaldbtime=totaldbtime+Duration.between(initialtime, Instant.now()).toMillis();
					
						stmt_log.executeUpdate("insert into svoc_log(customer_id,variable_name,error_desc) values('"
								+ runid + "','SVOC_Status','" + numberinsert + " Customer Processed. ')");

					}

					if (log_customer_id != 0) {
						stmt_log.executeUpdate("insert into svoc_log (customer_id,variable_name,error_desc) values ('"
								+ log_customer_id + "','" + log_variable_name + "','" + log_error + "')");
						log_customer_id = 0;
						log_variable_name.clear();
						log_error.clear();
					}

					sv.resetValues();
				} catch (Exception e) {
					sv.resetValues();
					System.out.println(
							"SVOC Generator error in Svoc or Entity Dictonary  Metadata is not loaded properly "
									+ e.getMessage());

				}
				
				if (CONSTANT.DEBUG )
				{
					System.out.println(" For Customer time taken " +  Duration.between(customer_time, Instant.now()).toMillis());
				}

			}
							  
			 
			
			try {
				initialtime = Instant.now();
			
			 
			
				stmt_pv.executeBatch();
				stmt_tp.executeBatch();
				stmt_cp.executeBatch();
				stmt_ap.executeBatch();
				stmt_fdv.executeBatch();
				stmt_fsv.executeBatch();
				stmt_bdder.executeBatch();

				con_log.commit();
				con_pv.commit();
				con_tp.commit();
				con_cp.commit();
				con_ap.commit();
				con_fdv.commit();
				con_fsv.commit();
				con_bdder.commit();
				
				 System.out.println("Thread :" + threadid + " Total Customers Persisted:" + numberinsert
								+ " Batch Size :" + CONSTANT.bacth_insert_count + " Time: "
								+ Duration.between(initialtime, Instant.now()).toMillis());
				 
				 exec_time=exec_time+ Duration.between(initialtime, Instant.now())
					.toMillis();

totaldbtime=totaldbtime+Duration.between(initialtime, Instant.now()).toMillis();
				
				stmt_log.executeUpdate("insert into svoc_log(customer_id,variable_name,error_desc) values('" + runid
						+ "','SVOC_Status','" + numberinsert + " Customer Processed. ')");

			} catch (Exception e) {
				System.out.println("SVOC Generator error in thread " + threadid + " " + e.getMessage());

			}
			


			

			finally {
				con_log.close();
				con_pv.close();
				con_tp.close();
				con_cp.close();
				con_ap.close();
				con_fdv.close();
				con_fsv.close();
				con_bdder.close();
				
																		
				
		/*		System.out.println(" Thread :" + threadid + " COMPLETED !!! Total DB Insert Time : "
			+totaldbtime+" Total DB Select Query Time : "
			+totalselectquerytime+" Total select query count :"
			+totalselectquerycount+" Total Execution Time :"
			+ Duration.between(threadinitialtime, Instant.now()).toMillis() + " Total Customers : "
			+ numberinsert + " Total Bills " + numberinsert_bdder); */
				
				System.out.println(" Thread :" + threadid + " COMPLETED !!! Select Query Time : "
						+select_time+" Variable Process Time: "+(var_time+batch_time)+" DB Insert Time :"+exec_time+" Total Time :"+(select_time+var_time+batch_time+exec_time) + " Total Customers : "
						+ numberinsert + " Total Bills " + numberinsert_bdder);
				
			}
				  
		
			}

		catch (Exception e) {
			System.out.println(" SVOC Generator error " + e.getMessage());
		}
		
	}
}
